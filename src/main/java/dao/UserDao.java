/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author Windows10
 */
public class UserDao implements DaoInterFace<User> {

//    private static ArrayList<User> userList = new ArrayList<>();
    private static User currentUser = null;

//    static {
//        //Mock data
//        userList.add(new User(1, "amy", "0868250489", "aaa", "aaa"));
//        userList.add(new User(1, "admin", "password", "admin1", "password"));
//        userList.add(new User(1, "admin", "password", "admin2", "password"));
//    }

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user (name, tel, username, password) VALUES (?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getUsername());
            stmt.setString(4, object.getPassword());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR ADD: " + ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, tel, username, password FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String username = result.getString("username");
                String password = result.getString("password");
                User user = new User(id, name, tel, username, password);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR GET ALL: " + ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, tel, username, password FROM user WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String username = result.getString("username");
                String password = result.getString("password");
                User user = new User(pid, name, tel, username, password);
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR GET: " + ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM user WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR DELETE: " + ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?, tel = ? , username = ?, password = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getUsername());
            stmt.setString(4, object.getPassword());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR UPDATE : " + ex);
        }

        db.close();
        return row;
    }

//    login
    public static User authenticate(ArrayList<User> list,String userName, String password) {
        
        for (User user : list) {
            if (user.getUsername().equals(userName) && user.getPassword().equals(password)) {
                currentUser = user; 
                return user;
            }
           
        }

        return null;
    }

    public static boolean isLogin() {
        return currentUser != null;
    }

    public static User getCurrentUser() {
        return currentUser;
    }
    
    public static void logout(){
        currentUser = null;
    }

    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println("User getAll: " + dao.getAll());
        System.out.println("User get id: " + dao.get(1));
        int id = dao.add(new User(-1, "Onanong", "096321789", "user04", "password04"));
        System.out.println("User get new id: " + dao.get(id));
    }
}
